﻿// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license 

using System.Diagnostics;
using System.IO;
using QRCoder;
using static QRCoder.QRCodeGenerator;

////
// Read data from user
//
System.Console.Write("\nEnter content to encode >");
string payload = System.Console.ReadLine();

////
// Encode data into QR code image
//
QRCodeGenerator qr = new QRCodeGenerator();
var qrCodeData = qr.CreateQrCode(payload, ECCLevel.M);
using var qrCode = new PngByteQRCode(qrCodeData);
byte[] data = qrCode.GetGraphic(20);
File.WriteAllBytes("qr.png", data);

////
// Open QR code image
//
Process.Start(new ProcessStartInfo
{
    FileName = "qr.png",
    UseShellExecute = true,
    WindowStyle = ProcessWindowStyle.Normal,
});
