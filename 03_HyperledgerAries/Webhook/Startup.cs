// Copyright (c) 2022 Sitekit Systems Ltd t/a Condatis. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license 

using System;
using System.IO;
using System.Text.Json;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Webhook
{
    public class Startup
    {
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                ////
                // Map all paths to this handler
                endpoints.MapFallback(async context =>
                {
                    Console.WriteLine("Notification received to {0}", context.Request.Path);

                    ////
                    // Use requested path as a folder for the file
                    string requestPath = context.Request.Path.ToString().TrimStart('/');
                    string directory = Path.Combine(env.ContentRootPath, "requests", requestPath);

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    ////
                    // Use time as a filename
                    string fileName = DateTime.Now.ToString("HHmm.ss.FF") + ".json";
                    string filePath = Path.Combine(directory, fileName);

                    ////
                    // Save request body into a file
                    var body = await JsonSerializer.DeserializeAsync<JsonElement>(context.Request.Body);
                    await File.WriteAllTextAsync(filePath, JsonSerializer.Serialize(body, new JsonSerializerOptions
                    {
                        WriteIndented = true,
                    }));

                    ////
                    // Respond to the caller
                    await context.Response.WriteAsync("Accepted");
                });
            });
        }
    }
}
